package com.example.madrental.metier.dto

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(tableName = "vehicules")
@Parcelize
data class VehiculeDTO(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    var nom: String? = null,
    var image: String? = null,
    var disponible: Int = 1,
    var prixjournalierbase: Int = 0,
    var categorieco2: String? = null) : Parcelable
