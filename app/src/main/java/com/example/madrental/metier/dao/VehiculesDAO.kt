package com.example.madrental.metier.dao

import androidx.room.*
import com.example.madrental.bo.Vehicule
import com.example.madrental.metier.dto.VehiculeDTO

@Dao
abstract class VehiculesDAO
{

    @Query("SELECT * FROM vehicules ORDER BY id")
    abstract fun getFavoriteVehicules(): List<Vehicule>

    @Insert
    abstract fun insert(vararg vehicule: VehiculeDTO)

    @Query("SELECT * FROM vehicules WHERE nom = :nom")
    abstract fun creationVerification(vararg nom: String): List<Vehicule>

    @Update
    abstract fun update(vararg vehicules: VehiculeDTO)

    @Query("DELETE FROM vehicules WHERE nom = :vehicule")
    abstract fun delete(vararg vehicule: String)
}