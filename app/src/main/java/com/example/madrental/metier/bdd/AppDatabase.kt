package com.example.madrental.metier.bdd

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.madrental.metier.dao.VehiculesDAO
import com.example.madrental.metier.dto.VehiculeDTO

@Database(entities = [VehiculeDTO::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase()
{
    abstract fun vehiculesDAO(): VehiculesDAO
}
