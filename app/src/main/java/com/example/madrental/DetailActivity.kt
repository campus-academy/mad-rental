package com.example.madrental

import android.content.Intent
import android.os.*
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.madrental.adapters.VehiculesAdapter
import com.example.madrental.bo.Vehicule
import com.example.madrental.metier.bdd.AppDatabaseHelper
import com.example.madrental.metier.dto.VehiculeDTO
import com.squareup.picasso.Picasso

class DetailActivity : AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.vehicule_main)

        val addBtn: Button = findViewById(R.id.add_favorite)
        val deleteBtn: Button = findViewById(R.id.delete)

        val name = getIntent().getExtras()?.getString("name")
        val image = getIntent().getExtras()?.getString("image")
        val price = getIntent().getExtras()?.getString("price")
        val category = getIntent().getExtras()?.getString("category")

        val textViewVehiculeLabel: TextView = findViewById(R.id.vehicule_label)
        val textViewVehiculePrice: TextView = findViewById(R.id.vehicule_price)
        val textViewVehiculeCategory: TextView = findViewById(R.id.vehicule_category)
        val imageView: ImageView = findViewById(R.id.image)

        textViewVehiculeLabel.text = name.toString()
        textViewVehiculePrice.text = price.toString() + " / jour"
        textViewVehiculeCategory.text = "Catégorie CO2: " + category.toString()

        val url = "http://s519716619.onlinehome.fr/exchange/madrental/images/" + image
        Picasso.get().load(url).into(imageView);

        val isFavorite = name != null && AppDatabaseHelper.getDatabase(this).vehiculesDAO().creationVerification(name).size > 0

        if (isFavorite == true) {
            addBtn.setVisibility(View.GONE)
        } else {
            deleteBtn.setVisibility(View.GONE)
        }
    }

    fun addToFavorite(view: View) {
        val name = getIntent().getExtras()?.getString("name")
        val image = getIntent().getExtras()?.getString("image")
        val price = getIntent().getExtras()?.getString("price")
        val category = getIntent().getExtras()?.getString("category")

        if (price != null) {
            val vehiculeDTO = VehiculeDTO(
                0,
                name,
                image,
                1,
                price.toInt(),
                category
            )

            if (name != null && AppDatabaseHelper.getDatabase(this).vehiculesDAO().creationVerification(name).size <= 0) {
                AppDatabaseHelper.getDatabase(this).vehiculesDAO().insert(vehiculeDTO)
                Toast.makeText(
                    this, "Véhicule ajouté en favoris",
                    Toast.LENGTH_SHORT
                ).show()
                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra("favorite", true)
                startActivity(intent)
            } else {
                Toast.makeText(
                    this, "Ce véhicule est déjà en favoris",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    fun deleteFavorite(view: View) {
        val name = getIntent().getExtras()?.getString("name")

        if (name != null) {
            AppDatabaseHelper.getDatabase(this).vehiculesDAO().delete(name)

            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("favorite", true)
            startActivity(intent)
        }
    }
}