package com.example.madrental

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.SwitchCompat
import com.example.madrental.metier.bdd.AppDatabaseHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.madrental.adapters.VehiculesAdapter
import com.example.madrental.bo.Vehicule
import com.example.madrental.business.ws.NetworkHelper
import com.example.madrental.business.ws.RetrofitSingleton
import com.example.madrental.business.ws.WSInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    private lateinit var vehiculesAdapter: VehiculesAdapter
    private lateinit var vehiculesRecyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // recyclerView :
        vehiculesRecyclerView = findViewById(R.id.vehiculesList)

        // à ajouter pour de meilleures performances :
        vehiculesRecyclerView.setHasFixedSize(true)

        // layout manager, décrivant comment les items sont disposés :
        val layoutManager = LinearLayoutManager(this)
        vehiculesRecyclerView.layoutManager = layoutManager



        val switchBtn = findViewById<SwitchCompat>(R.id.bouton_favorite)
        val fromFavorite = intent.getBooleanExtra("favorite", false)

        if (fromFavorite) {
            switchBtn.isChecked = true

            val vehiculesList: List<Vehicule> = AppDatabaseHelper.getDatabase(this).vehiculesDAO().getFavoriteVehicules();
            vehiculesAdapter = VehiculesAdapter(vehiculesList.toMutableList())
            vehiculesRecyclerView.adapter = vehiculesAdapter

            if (vehiculesList.size <= 0) {
                findViewById<TextView>(R.id.empty_fav).setVisibility(View.VISIBLE)
            } else findViewById<TextView>(R.id.empty_fav).setVisibility(View.GONE)

        } else {
            findViewById<TextView>(R.id.empty_fav).setVisibility(View.GONE)
            this.getVehicules()
        }

        switchBtn.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                val vehiculesList: List<Vehicule> = AppDatabaseHelper.getDatabase(this).vehiculesDAO().getFavoriteVehicules();
                vehiculesAdapter.updateVehicules(vehiculesList.toMutableList())

                if (vehiculesList.size <= 0) {
                    findViewById<TextView>(R.id.empty_fav).setVisibility(View.VISIBLE)
                } else findViewById<TextView>(R.id.empty_fav).setVisibility(View.GONE)

            } else {
                findViewById<TextView>(R.id.empty_fav).setVisibility(View.GONE)
                this.getVehicules()
            }
        }
    }

    fun checkNetwork()
    {
        if (!NetworkHelper.isConnected(this))
        {
            Toast.makeText(this, "Aucune connexion internet !", Toast.LENGTH_LONG).show()
            return
        }
    }

    fun getVehicules()
    {
        this.checkNetwork()

        // appel :
        val service = RetrofitSingleton.retrofit.create(WSInterface::class.java)
        val call = service.getVehicules()
        return call.enqueue(object : Callback<List<Vehicule>>
        {
            override fun onResponse(call: Call<List<Vehicule>>, response: Response<List<Vehicule>>)
            {
                if (response.isSuccessful)
                {
                    val vehiculesList: List<Vehicule>? = response.body()

                    if (vehiculesList != null) {
                        if (vehiculesList.size > 0)
                            vehiculesAdapter = VehiculesAdapter(vehiculesList.toMutableList())
                    }
                    vehiculesRecyclerView.adapter = vehiculesAdapter

                }
            }
            override fun onFailure(call: Call<List<Vehicule>>, t: Throwable)
            {
                Log.e("MainActivity", "${t.message}")
            }
        })
    }
}