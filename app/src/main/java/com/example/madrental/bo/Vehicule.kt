package com.example.madrental.bo

class Vehicule {
    var id: Long? = null
    var nom: String? = null
    var image: String? = null
    var disponible: Int = 1
    var prixjournalierbase: Int = 0
    var categorieco2: String? = null
}