package com.example.madrental.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.madrental.DetailActivity
import com.example.madrental.R
import com.example.madrental.bo.Vehicule
import com.squareup.picasso.Picasso

class VehiculesAdapter(
    private var vehiculesList: MutableList<Vehicule>,
) : RecyclerView.Adapter<VehiculesAdapter.VehiculeViewHolder>()
{
    // Crée chaque vue item à afficher :
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VehiculeViewHolder
    {
        val viewVehicule = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_vehicule, parent, false)

        return VehiculeViewHolder(viewVehicule)
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.vehicule_main
    }

    // Renseigne le contenu de chaque vue item :
    override fun onBindViewHolder(holder: VehiculeViewHolder, position: Int)
    {
        holder.textViewVehiculeLabel.text = vehiculesList[position].nom
        holder.textViewVehiculePrice.text = vehiculesList[position].prixjournalierbase.toString() + " / jour"
        holder.textViewVehiculeCategory.text = "Catégorie CO2: " + vehiculesList[position].categorieco2.toString()

        val url = "http://s519716619.onlinehome.fr/exchange/madrental/images/" + vehiculesList[position].image
        Picasso.get().load(url).into(holder.imageView);
    }

    override fun getItemCount(): Int = vehiculesList.size

    fun updateVehicules(listeVehicules: MutableList<Vehicule>) {
        this.vehiculesList = listeVehicules
        notifyDataSetChanged()
    }
    inner class VehiculeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val textViewVehiculeLabel: TextView = itemView.findViewById(R.id.vehicule_label)
        val textViewVehiculePrice: TextView = itemView.findViewById(R.id.vehicule_price)
        val textViewVehiculeCategory: TextView = itemView.findViewById(R.id.vehicule_category)
        val imageView: ImageView = itemView.findViewById(R.id.image)

        init
        {
            itemView.setOnClickListener {

                val intent = Intent(itemView.context, DetailActivity::class.java)

                intent.putExtra("name", vehiculesList[position].nom)
                intent.putExtra("price", vehiculesList[position].prixjournalierbase.toString())
                intent.putExtra("category", vehiculesList[position].categorieco2.toString())
                intent.putExtra("image", vehiculesList[position].image)

                itemView.context.startActivity(intent)
            }
        }
    }
}